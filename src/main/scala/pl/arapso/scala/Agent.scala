package pl.arapso.scala

import akka.actor.Actor
import pl.arapso.scala.Exchange._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
/**
  * Agent class
  *
  * Handles request messages from exchange
  * Answer with Response with random price between 1 to 20
  *
  * schedulers
  * - Cancel message will remove all pending responses which did not win
  */
class Agent(val duration: Long, val budget: Long) extends Actor {
  import Agent._

  def generatePrice(max: Int) : Int = 5//Random.nextInt(max)
  var available: Long = budget
  var reserved: Long = budget
  var currentTime: Long = 1
  var outgoingResponses: List[Response] = Nil
  val maxAmountPerRangeUnit = budget / duration

  context.system.scheduler.schedule(0.second, 5.second, self, Debug)
  context.system.scheduler.schedule(2.second, 2.second, self, Cancel)

  override def receive: Receive = {
    case Request(id, time) => {
      currentTime = time
      if(currentTime <= duration && available > 0) {
        print(s"-")
        val price = generatePrice(20)
        // if we try to send more then we have
        val tempPrice: Long = if(price > reserved) reserved else price
        if(tempPrice > 0) {
          reserved -= tempPrice
          val response = new Response(id, time, tempPrice);
          sender() ! response
          outgoingResponses = response :: outgoingResponses
        }
      } else {
        println(s"Current ${currentTime}")
      }
    }

    case Won(id, time, paid) => {
      println(s"!${paid}");
      outgoingResponses = outgoingResponses.filterNot(r => r.id == id)
      available -= paid
    }

    case Cancel => {
      outgoingResponses.filterNot(r => r.time > currentTime - 2).foreach(r => {
        reserved = reserved + r.price
      })
      outgoingResponses = outgoingResponses.filter(r => r.time > currentTime - 2)
    }

    case Debug => {
      println(s"\nR: ${reserved} A: ${available}")
    }


  }
}

object Agent {
  case class Debug()
  case class Cancel()
}
