package pl.arapso.scala

import akka.actor.{ActorSystem, Props}
import pl.arapso.scala.Exchange._

// TODO Add unit tests
object ScalaApp extends App {
  val system = ActorSystem("ad-exchange-system")

  val agent1 = system.actorOf(Props(new Agent(100, 2000)), name = "agent1")

  // TODO Unregister agents from exchange
  val exchange = system.actorOf(Props[Exchange], name = "exchange")
  exchange ! Register(agent1)

}