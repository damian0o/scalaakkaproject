package pl.arapso.scala

import akka.actor.{Actor, ActorRef}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import Math._

/**
  * Exchange Actor
  * Handle messages
  * Register - Registration of new agents
  * Response Agents responses to requests
  *
  * Sending messages
  * Request - simple offer
  * Won - information about win
  * Sending n requests to agents every second
  */
class Exchange extends Actor {
  import Exchange._

  var agents: List[ActorRef] = Nil
  var counter: Int = 1

  def r(t: Long): Int = (abs(cos((t / PI) - PI) + sin((t / 2 * PI) + PI)) * 100).toInt
  def p(t: Long, p: Long): Double = abs(sin(t/(2*PI)))

  context.system.scheduler.scheduleOnce(1.second, self, Tick(1))

  override def receive: Receive = {
    case  Register(agent)=> {
      agents = agent :: agents
    }
    case Tick(i) => {
        (1 to r(i)).foreach(
          requestId => {
            counter += 1
            agents.foreach(
              _ ! new Request(counter, i)
            )
          }
      )
      context.system.scheduler.scheduleOnce(1.second, self, Tick(i + 1))
    }
    case Response(id, time, price) => {
      // TODO should check if one of the agents already won
      if(scala.util.Random.nextDouble() < p(time, price)) {
        sender() ! new Won(id, time, price)
      }
    }
  }
}

object Exchange {
  case class Register(val agent: ActorRef)
  case class Request(val id:  Int, val time: Long)
  case class Response(val id: Int, val time: Long, val price: Long)
  case class Won(val id: Int, val time: Long, val paid: Long)
  case class Tick(val time: Long)
}