name := "ScalaAkka"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.4.10"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.4.10"
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "3.0.0" % "test"